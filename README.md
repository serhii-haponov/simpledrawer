# Drawing images with opportunity set parameters to line (color, opacity, thickness) #
![Screenshot 2017-04-26 15.01.01.png](https://bitbucket.org/repo/oLLAr6z/images/1105606790-Screenshot%202017-04-26%2015.01.01.png)
![Screenshot 2017-04-26 15.02.00.png](https://bitbucket.org/repo/oLLAr6z/images/838250062-Screenshot%202017-04-26%2015.02.00.png)

# Saving canvas to gallery #
![Screenshot 2017-04-26 15.07.53.png](https://bitbucket.org/repo/oLLAr6z/images/1742216947-Screenshot%202017-04-26%2015.07.53.png)
![Screenshot 2017-04-26 15.08.08.png](https://bitbucket.org/repo/oLLAr6z/images/3967656147-Screenshot%202017-04-26%2015.08.08.png)